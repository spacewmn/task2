const createBoard = () => {
    let board = '';
    for (let i = 0; i < 72; i++) {
        if (i % 9 === 0) {
            board += '\n';
        } else if (i % 2 !== 0) {
            board += '*';
        } else {
            board += ' ';
        }
    }
    return board;
}
console.log(createBoard())
